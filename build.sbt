val thisScalaVersion = "2.12.6"

val root = Project("root", file("."))
  .settings(
      name := "medidata",
      scalaVersion := thisScalaVersion,

      libraryDependencies ++= Seq(
        "org.scalatest" %% "scalatest" % "3.0.4" % "test",
        "org.scalacheck" %% "scalacheck" % "1.13.5" % "test"
      )
  )
