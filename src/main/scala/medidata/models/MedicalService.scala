package medidata.models

// Services we provide
sealed trait MedicalService

object MedicalService {

  case object Diagonsis extends MedicalService
  case object XRay extends MedicalService
  case object BloodTest extends MedicalService
  case object ECG extends MedicalService
  final case class Vaccine(count: Int) extends MedicalService

}
