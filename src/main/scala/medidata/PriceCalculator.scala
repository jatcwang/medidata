package medidata
import medidata.models.MedicalService
import medidata.models.MedicalService._

object PriceCalculator  {
  /**
    * Calculate the base price for a given service
    */
  def calcBasePrice(service: MedicalService): Double = {
    service match {
      case Diagonsis => 60
      case XRay => 150
      case BloodTest => 78
      case ECG => 200.40
      case Vaccine(count: Int) => 27.50 + count * 15
    }
  }

  /**
    * Calculate the discount based on age.
    * @return The multiplier to apply the discount (e.g. 90% off == 0.1)
    */
  def calcAgeMultiplier(age: Int): Double = {
    if (age >= 70) {
      0.1
    }
    else if (age >= 65) {
      0.4
    }
    else if (age < 5) {
      0.6
    }
    else {
      1 // no discount
    }
  }

  /**
    * Calculate discount based on service and whether the person has MediHealth insurance
    * @return multiplier for the price (to apply the discount)
    */
  def calcServiceInsuranceMultipler(service: MedicalService, hasMediHealthInsurance: Boolean): Double  = {
    // Assuming all services are diagnosed by a MediHealth practitioner and thus anyone with
    // MediHealth insurance gets additional discount
    if (hasMediHealthInsurance && service == BloodTest) 0.85 else 1
  }

  /**
    * Calculate the price of a service, considering age and insurance discount.
    */
  def calcPrice(service: MedicalService, age: Int, hasMediHealthInsurance: Boolean) = {

    calcBasePrice(service) * calcAgeMultiplier(age) * calcServiceInsuranceMultipler(service, hasMediHealthInsurance)

  }

}
