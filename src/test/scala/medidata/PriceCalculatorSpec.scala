package medidata

import medidata.PriceCalculator.{calcAgeMultiplier, calcBasePrice, calcPrice, calcServiceInsuranceMultipler}
import medidata.PriceCalculatorSpec._
import medidata.models.MedicalService
import medidata.models.MedicalService._
import org.scalacheck.Gen
import org.scalatest.prop._
import org.scalatest.{FreeSpec, Matchers}

class PriceCalculatorSpec
  extends FreeSpec
  with GeneratorDrivenPropertyChecks
  with Matchers {

  "calcAgeMultiplier" - {
    "returns the right multiplier" in {
      Seq(
        75 -> 0.1,
        70 -> 0.1,
        69 -> 0.4,
        65 -> 0.4,
        64 -> 1.0,
        30 -> 1.0,
        5 -> 1.0,
        4 -> 0.6,
      ).map { case (age, expectedMultiplier) =>
        withClue(s"age ${age}:") {
          calcAgeMultiplier(age) shouldEqual expectedMultiplier
        }
      }
    }
  }

  "calcBasePrice" - {
    "returns the right base price for given service" in {
      Seq(
        Diagonsis -> 60,
        XRay -> 150,
        BloodTest -> 78,
        ECG -> 200.40,
        Vaccine(5) -> 102.5,
        Vaccine(1) -> 42.50,
      ).map { case (service, expectedBasePrice) =>
        withClue(s"service ${service}") {
          calcBasePrice(service) shouldEqual expectedBasePrice
        }
      }
    }
  }

  "calcServiceInsuranceMultipler" - {
    "returns the correct multiplier given service chosen and insurance condition" in {
      Seq(
        (BloodTest, true) -> 0.85,
        (BloodTest, false) -> 1,
        (XRay, true) -> 1,
        (Vaccine(1), false) -> 1,
      ).map { case ((service, hasMediHealthInsurance), expectedMultiplier) =>
        withClue(s"service ${service} and insurance status ${hasMediHealthInsurance}") {
          calcServiceInsuranceMultipler(service, hasMediHealthInsurance) shouldEqual expectedMultiplier
        }
      }
    }
  }

  "calcPrice calculates the correct price for" - {
    "Age 70, BloodTest with insurance" in {
      calcPrice(BloodTest, 70, hasMediHealthInsurance = true).roundTo2 shouldEqual 6.63
    }

    "Age 65, XRay with insurance" in {
      calcPrice(XRay, 65, hasMediHealthInsurance = true).roundTo2 shouldEqual 60
    }

    "Age 30, Vaccine(5)" in {
      calcPrice(Vaccine(5), 30, hasMediHealthInsurance = false).roundTo2 shouldEqual 102.5
    }

    "Age 4, Diagnosis" in {
      calcPrice(Diagonsis, 4, hasMediHealthInsurance = false).roundTo2 shouldEqual 36
    }

  }

  "Property: Applying discount should always be cheaper (or same price) as the base price" in {
    forAll(genMedialService, Gen.choose(0, 120), Gen.oneOf(true, false)) {
      case (service, age, hasMediHealthInsurance) =>
        assert(calcPrice(service, age, hasMediHealthInsurance) <= calcBasePrice(service))
    }
  }

}

object PriceCalculatorSpec {
  // extension methods to help with test
  implicit final class BoubleExt(val num: Double) extends AnyVal {
    def roundTo2: Double = BigDecimal(num).setScale(2, BigDecimal.RoundingMode.HALF_UP).toDouble
  }

  implicit val genMedialService: Gen[MedicalService] = {
    Gen.oneOf(
      Gen.const(Diagonsis),
      Gen.const(XRay),
      Gen.const(BloodTest),
      Gen.const(ECG),
      for {
        count <- Gen.posNum[Int]
      } yield Vaccine(count)
    )
  }

}
